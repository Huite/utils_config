@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
choco feature enable -n allowGlobalConfirmation
choco install qgis
choco install cmder
choco install git
choco install 7zip
choco install brave
choco install bitwarden
choco install mpv
choco install atom
choco install vscode
choco install pycharm-community
choco install notepadplusplus
choco install tortoisesvn
choco install zotero
choco install paraview
choco install inkscape
choco install ripgrep
choco install ripgrep-all

:: manually install neovim? For right Python bindings
:: install TotalCommander from appstore (license)
:: install julia
:: install miniconda
:: install iMOD
:: install VisualStudio (add license again?)
:: install VisualFortran (check license too)
:: install Docker
:: install MPICH, right version
:: Maybe autohotkey? Setup doesn't currently work for totalcmd...
:: Prepare conda environment.yml, take imod example


