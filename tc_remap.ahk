#SingleInstance ignore
#IfWinActive ahk_class TTOTAL_CMD
+j::
+k::
{
	aKey := A_ThisHotKey == "+j" ? "Down" : "Up"  
	ControlGetFocus, aControl, A
	if (RegExMatch(aControl, "LCLListBox[1-2]"))
		Send, {%aKey%}
	else
		Send, {%A_ThisHotKey%}
return
}
+d::
+u::
{
	aKey := A_ThisHotKey == "+d" ? "PgDn" : "PgUp"  
	ControlGetFocus, aControl, A
	if (RegExMatch(aControl, "LCLListBox[1-2]"))
		Send, {%aKey%}
	else
		Send, {%A_ThisHotKey%}
return
}
