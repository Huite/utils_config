# HOW TO: Setup workspace

## Installation

* Install anaconda base environment via miniconda
	- https://conda.io/miniconda.html
* Create environment "main": `conda env create -f environment.yml`


Where environment.yml:

```
name: main

channels:
  - conda-forge
  - defaults
  - conda-forge/label/dev

dependencies:
  - python>=3.6
  - numpy
  - xarray>=0.10
  - numba
  - pandas
  - geopandas
  - dask
  - rasterio>=1.0a12
  - git
  - git-lfs
  - jupyterlab
  - spyder
  - cookiecutter
  - earthengine-api
```

* Install cygwin_64
* Install ConsoleZ: https://github.com/cbucher/console/wiki/Downloads
* Install cmder_mini: https://cmder.net
* Install lf: https://godoc.org/github.com/gokcehan/lf
* Install autojump: https://github.com/wting/autojump # replace by fasder
* Install VSCode (portable): https://code.visualstudio.com/docs?dv=winzip
* Install neovim: https://neovim.io/
* Install AutoHotKey: https://autohotkey.com/

## Configuration
# Shortcuts
Create shortcuts in folder that's added to %PATH%:

TotalCommander
Copy WINCMD.INI to %APPDATA%/GHISLER

AutoHotkey help
```
#IfWinActive ahk_class TTOTAL_CMD
j::
k::
{
	aKey := A_ThisHotKey == "j" ? "Down" : "Up"  
	ControlGetFocus, aControl, A
	if (RegExMatch(aControl, "LCLListBox[1-2]"))
		Send, {%aKey%}
	else
		Send, {%A_ThisHotKey%}
return
}
d::
u::
{
	aKey := A_ThisHotKey == "d" ? "PgDn" : "PgUp"  
	ControlGetFocus, aControl, A
	if (RegExMatch(aControl, "LCLListBox[1-2]"))
		Send, {%aKey%}
	else
		Send, {%A_ThisHotKey%}
return
}
```
Compile it with autohotkey into tc_remap.exe.


VSCode
```
start /b d:\bootsma\utils\VSCode-win32-x64-1.25.1\Code.exe
```

Cmder
```
CD /D %~dp1
CALL activate main
start /b d:\bootsma\utils\cmder_mini\Cmder.exe
```

Neovim
```
d:\bootsma\utils\Neovim\bin\nvim-qt.exe %*
```

Install Plug
```
md ~\AppData\Local\nvim\autoload
$uri = 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
(New-Object Net.WebClient).DownloadFile(
  $uri,
  $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath(
    "~\AppData\Local\nvim\autoload\plug.vim"
  )
)
```

Add init.vim to ~/AppData/Local/nvim:

```
call plug#begin('$LOCALAPPDATA/nvim')
Plug iCyMind/NeoSolarized'
call plug#end()

syntax enable
colorscheme NeoSolarized
```

**Note**: Plug needs git to function.

lf
```
@echo off
rem Change working dir in cmd.exe to last dir in lf on exit.
rem
rem You need to put this file to a folder in %PATH% variable.

:tmploop
set tmpfile="%tmp%\lf.%random%.tmp"
if exist "%tmpfile%" goto:tmploop
lf -last-dir-path="%tmpfile%" %*
if not exist "%tmpfile%" exit
set /p dir=<"%tmpfile%"
del /f "%tmpfile%"
if not exist "%dir%" exit
rem if "%dir%" == "%cd%" exit
cd "%dir%"
```

### Additional
Cmder, Win+Alt+P to settings

Add Task under Startup: {multi} (or something) to "split" current pane (opens new tab)
```
cmd.exe /k -cur_console:s2THn ""%ConEmuDir%\..\init.bat""
```

ConsoleZ: change background color (last menu, tabs), remove bars, change selection color, change <colors> in xml to SolarizedLight.

### Kakoune (needs Cygwin, doesn't load rc properly it seems)
* Clone from github
* Make sure G++ is installed on cygwin
* Install asciidoc (cygwin-setup)
* Install ncurses-devel (cygwin-setup)
* In kakoune/src, run "make" to build
* rename kak.debug (e.g.) to kak.exe
